

YUI().use(
        'node', 'app-base', 'nahovno-views',
        function (Y) {

            var startApp = function () {

                try {

                    // cordova
//                    var db=null;
                    var db = window.sqlitePlugin.openDatabase({name: 'nahovno02.db', location: 'default'});


                    var app = new Y.App({
                        views: {
                            "frontpage": {type: "NahovnoFrontpageView"},
                            "detail": {type: "NahovnoDetailView", parent: "frontpage"}
                        },
                        root: "/",
                        serverRouting: false,
                        viewContainer: '#container',
                        container: '#container',
                        transitions: false,
                    });

                    app.route('/', function (req, res, next) {
                        app.showView('frontpage', {"database": db, "app": app});
                    });

//                    app.render().dispatch().save('/');

                    db.executeSql('CREATE TABLE IF NOT EXISTS NahovnoTable (timestamp varchar(48), rating int, fileurl varchar(255))', []
                            , function () {
                                try {
                                    app.render().dispatch().save('/');
                                } catch (e) {
                                    alert('initial dispatch err ' + e.message);
                                }
                            }, function (error) {
                        alert('SQL create error: ' + JSON.stringify(error));
                    });
                    
                } catch (e) {
                    alert('Unable to start the app: ' + e.stack);
                }
                
                

            };

            var cordovaApp = {
                // Application Constructor
                initialize: function () {
                    this.bindEvents();
                },
                bindEvents: function () {
                    document.addEventListener('deviceready', this.onDeviceReady, false);
                },
                onDeviceReady: function () {
//                    window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
                    startApp();
                }
            };

            cordovaApp.initialize();

        });