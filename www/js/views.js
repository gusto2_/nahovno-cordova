YUI.add('nahovno-views', function (Y) {

    function alertMessage(msg) {
        if (msg)
            Y.one('#message').setHTML(Y.Escape.html(msg));
        else
            Y.one('#message').setHTML(Y.Escape.html(''));
    }

    Y.RecordModel = Y.Base.create('recordModel', Y.Model, [], {
        ATTRS: {
            /** 
             * timestamp of the record
             * for simplyfication we can use the textual representation
             **/
            timestamp: {
                value: 0 // default value
            },
            /**
             * rating from 1 to 5, 0 is 'no value'
             */
            rating: {
                value: 0
            },
            photoURL: {
                value: ''
            }
        }
    });


    // front page view
    Y.NahovnoFrontpageView = Y.Base.create('NahovnoFrontpageView', Y.View, [], {
        datasource: new Y.DataSource.Function({
            // request is a json array
            source: function (request) {
                return request;
            }
        }),
        render: function () {
            try {
                var container = this.get('container');
                var db = this.get('database');
                var app = this.get('app');

                var context = this;
                container.setHTML(Y.one('#nahovnoListSrc').getHTML());
                context.loadPageElements(container, context);
                context.readData(db, context);

                var newButton = container.one('#newButton');
                newButton.on('click', function (evt) {
                    context.switchToDetail(db, app, new Y.RecordModel());
                });

            } catch (e) {
                alert('NahovnoFrontpageView render ' + e.message);
            }
            return this;

        },
        /**
         * 
         * @param {type} record (or en empty record if new)
         * @returns {undefined}
         */
        switchToDetail: function (db, app, detail) {

            try {
                if (!app) {
                    alert('No app defined');
                    return;
                }
                app.showView('detail', {"database": db, "app": app, "record": detail});
            } catch (e) {
                alert("switchToDetail " + e.message);
            }

        },
        loadPageElements: function (container, context) {

            try {
                var myDatasource = context.datasource;
                myDatasource.plug(Y.Plugin.DataSourceJSONSchema, {
                    schema: {
                        resultListLocator: 'data',
                        resultFields: [
                            "timestamp",
                            "rating",
                            "fileurl"
                        ]
                    }
                });

                var table = new Y.DataTable(
                        {
                            columns: ['timestamp', 'rating']
                        });

                table.plug(Y.Plugin.DataTableDataSource, {
                    datasource: myDatasource
                });

                table.render(container.one('#listTable'));
                table.set('id', 'nahovnoListTable');
                table.delegate('tap', function (evt) {
                    var record = table.getRecord(evt.target);
                    if (!record) {
                        return;
                    }
                    var app = context.get('app');
                    var db = context.get('database');
                    try {
                        evt.preventDefault();
                        evt.stopPropagation();
                        context.switchToDetail(db, app, record);

                    } catch (e) {
                        alert('list tap ' + e.message);
                    }

                }, "td");
                context.table = table;
            } catch (e) {
                alert("Unable to build ui: " + e.message);
            }
        },
        readData: function (db, context) {

            // test browser

            try {

//                var data = [
//                    { timestamp: '2016-09-26 17:22', rating: 1, fileurl: '' },
//                    { timestamp: '2016-09-26 17:22', rating: 1, fileurl: '' }
//                ];
//                context.table.datasource.load({request: {"data": data}});

                db.executeSql('SELECT timestamp, rating, fileurl FROM NahovnoTable order by timestamp desc', [], function (rs) {
                    var data = [];
                    for (var i = 0; i < rs.rows.length; i++) {
                        var d = rs.rows.item(i);
                        data.push(d);
                    }
                    try {
                        context.table.datasource.load({request: {"data": data}});
                    } catch (ex) {
                        alertMessage('readData: ' + ex.message + ' ' + ex.stack);
                    }
                }, function (error) {
                    alert('SELECT SQL statement ERROR: ' + error.message);
                });

                // catch
            } catch (e) {
                alert('read data err: ' + e.stack);

            }
        }

    });


    Y.NahovnoDetailView = Y.Base.create('NahovnoDetailView', Y.View, [], {
        render: function () {
//            alert('render detail start');
            try {
                var record = this.get('record');
                if (!record) {
                    alert('NahovnoDetailView record is undefined');
                    record = new Y.RecordModel();
                }
//                alertMessage("NahovnoDetailView record: " + JSON.stringify(record));

                var container = this.get('container');
                if (!container) {
                    alert('NahovnoDetailView render container is undefined');
                    return;
                }
                var db = this.get('database');
                if (!db) {
                    alert('NahovnoDetailView render db is undefined');
                }
                var app = this.get('app');
                if (!app) {
                    alert('NahovnoDetailView render app is undefined');
                }

                var context = this;
                try {

//                    container.setHTML();
                    container.setHTML(Y.one('#nahovnoDetailSrc').getHTML());
                } catch (e) {
                    alert('NahovnoDetailView render unable to set content');
                }
//                if (!container.inDoc()) {
//                    Y.one('#container').append(container);
//                }


                try {

                    if (record) {
                        // alertMessage(record);
                        var jsonRecord = record;
                        var timestamp = new Date().toISOString();
                        if (jsonRecord.get('timsestamp')) {
                            timestamp = jsonRecord.get('timsestamp');
                        }
                        container.one('#timestampField').set('value', timestamp);

                        if (jsonRecord.get('rating')) {
                            container.one('#ratingField').set('value', jsonRecord.get('ratingField'));
                        }
                        var fileurl = jsonRecord.get('fileurl');
                        if (fileurl && fileurl != '') {
                            var imgNode = container.one('#smallImage');
                            imgNode.set('style.display', 'block');
                            imgNode.set('src', fileurl);
                            
                            var sharePictureButton = container.one('#sharePictureButton');
                            sharePictureButton.set('style.display', 'block');
                            sharePictureButton.on('click', function (e) {
                                context.sharePicture(fileurl);
                            });
                        }
                    }
                } catch (e) {
                    alert('NahovnoDetailView render unable to set values ' + e.stack);
                }

            } catch (e) {
                alert('NahovnoDetailView render err ' + e.message + ' ' + e.stack);
            }
//            alert('render detail end');
            return this;
        },
        saveRecord: function () {
            try {
                var container = this.get('container');
                if (!container) {
                    alert('saveRecord, this container is not in context');
                }
                var db = this.get('database');
                var context = this;

                var timestamp = container.one('#timestampField').get('value');
                var rating = container.one('#ratingField').get('value');
                var fileurl = container.one('#smallImage').get('src');
                if (!fileurl || fileurl.indexOf(".html") > -1) {
                    fileurl = '';
                }

                db.executeSql('INSERT INTO NahovnoTable (timestamp, rating, fileurl) VALUES (?,?, ?)',
                        [timestamp, rating, fileurl]
                        , function () {
                            try {
                                context.returnToList();
                            } catch (e) {
                                alert('inserting new values: ' + e.message);
                            }
                        }, function (error) {
                    alert('SQL create error: ' + JSON.stringify(error));
                });

            } catch (e) {
                alert('saveRecord: ' + e.stack);
            }
        },
        returnToList: function (e) {
//            alert('canceling');

            var app = this.get('app');
            if (!app) {
                alert('NahovnoDetailView returnToList app is undefined!');
                return;
            }
            var db = this.get('database');
            if (!app) {
                alert('NahovnoDetailView returnToList database is undefined!');
                return;
            }

            alertMessage('');
            app.showView('frontpage', {"database": db, "app": app});
            if (e) {
                e.preventDefault();
            }
        },
        takePicture: function () {

            var container = this.get('container');
            var context = this;

            var onPhotoDataSuccess = function (fileURL) {
                try {
//                    alertMessage('onPhotoDataSuccess ' + fileURL);

                    var imgNode = container.one('#smallImage');
                    imgNode.set('style.display', 'block');
                    imgNode.set('src', fileURL);

                    var sharePictureButton = container.one('#sharePictureButton');
                    sharePictureButton.set('style.display', 'block');
                    sharePictureButton.on('click', function (e) {
                        context.sharePicture(fileURL);
                    });

                } catch (e) {
                    alertMessage('onPhotoDataSuccess ' + e.stack);
                }
            };

            try {

                var context = this;
                var pictureSource = navigator.camera.PictureSourceType;
                var destinationType = navigator.camera.DestinationType;

                navigator.camera.getPicture(
                        onPhotoDataSuccess,
                        null,
                        {
                            quality: 50,
                            destinationType: destinationType.FILE_URL,
                            correctOrientation: true
                        }
                );

            } catch (e) {
                alertMessage('takePicture ' + e.stack);
            }
        },
        // share picture
        sharePicture: function (url) {
            try {
                // this is the complete list of currently supported params you can pass to the plugin (all optional)
                var shareOptions = {
//                    message: 'share this', // not supported on some apps (Facebook, Instagram)
//                    subject: 'the subject', // fi. for email
                    files: [url] // an array of filenames either locally or remotely
//                    url: 'https://www.website.com/foo/#bar?a=b',
//                    chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
                };

                window.plugins.socialsharing.shareWithOptions(
                        shareOptions,
                        function () {
                            alertMessage('successfully shared');
                        },
                        function (err) {
                            alertMessage('error sharing ' + e.message);
                        });

            } catch (e) {
                alertMessage('share picture ' + e.message);
            }
        },
//        onPhotoDataSuccess: function(fileURL) {
//            try {
//                alertMessage('onPhotoDataSuccess '+fileURL);
//                var container = this.get('container');
//                var imgNode = container.one('#smallImage');
//                imgNode.set('style.display', 'block');
//                imgNode.set('src', fileURL);
//                
//                alert('The photo should be displayed');
//            }
//            catch(e) {
//                alert('onPhotoDataSuccess '+e.stack);
//            }
//        },

        onPhotoDataFail: function (e) {
            alertMessage('foto failed ' + e);
        },
        events: {
            '#cancelButton': {click: "returnToList"},
            '#takePictureButton': {click: "takePicture"},
            '#saveButton': {click: "saveRecord"}
        }
    });
},
        '0.0.1',
        {
            requires: [
                'view', 'node-load', 'datatable-base', 'model', 'escape',
                'node-event-delegate', 'datasource-function', 'datatable-datasource',
                'datatable', 'event-tap', 'datasource-jsonschema'
            ]
        }
);

